import re
from html.parser import HTMLParser


class Tree(dict):
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __init__(self, data=None, parent=None):
        super().__init__()
        self.data = []
        self.parent = parent

        self.update(data if data else [])

    def add_child(self, data=None):
        child = Tree(data)
        child.parent = self

        self.data.append(child)
        return child

    def compare_tags(self, tags_mas):
        for tag in tags_mas:
            if self.compare_tag(tag):
                return True

        return False

    def compare_tag(self, tag_name):
        """
        Решает: соответствует ли текущий тег заданному фильтру
        """
        # Тег элемента пустой
        if 'tag' not in self or self.tag == '':
            return False

        param = tag_name.split('.')

        # тег без требований класса
        if len(param) == 1:
            return self.tag == param[0]

        # проверяем, чтобы все заданные классы хоть как-то содержались в теге
        if 'class' in self.attr:
            count = 1

            for cl in param[1:]:
                if cl in self.attr['class']:
                    count += 1

            return count == len(param)
        return False


class Parser(HTMLParser):
    def __init__(self, page_text, config):
        super().__init__()
        self.config = config

        self.data = Tree()
        self.link = self.data
        self.root = None
        self.title = None

        self.feed(page_text)

    def error(self, message):
        pass

    def handle_starttag(self, tag, attrs):
        if self.link is None:
            return

        if tag in self.config["parser"]['ignore_line_tags']:
            return

        self.link = self.link.add_child(dict(tag=tag, attr=dict(attrs)))

    def handle_endtag(self, tag):
        if tag in self.config["parser"]['ignore_line_tags']:
            return

        while self.link is not None and self.link.tag != tag:
            self.link = self.link.parent
        if self.link is not None:
            self.link = self.link.parent

    def handle_data(self, data):
        if self.link is None or 'tag' not in self.link:
            return
        self.link.data.append(self._str_format(data))

    def _optimization(self, tag_item):
        """
        Убираем ненужные теги
        """
        tag_item.data = [item for item in tag_item.data
                         if type(item) == str or not item.compare_tags(self.config["parser"]["cancel_tags"])]

        for child in tag_item.data:
            if type(child) is Tree:
                self._optimization(child)

    def find_root(self):
        self.title = self._find_root('title')

        for root_tag in self.config["parser"]["root"]:
            res = self._find_root(root_tag)

            self.root = res if self.root is None else self.root

        self._optimization(self.root)

    def _find_root(self, tag_name, link=None):
        """
        Назначаем корневой тег
        """
        if self.root is not None:
            return

        # печатаем текущий искомый тег
        if link is None:
            link = self.data
            print(f"  '{tag_name}'...")

        if link.compare_tag(tag_name) and not link.compare_tags(self.config["parser"]["cancel_tags"]):
            print('- успешно')
            return link

        for child in link.data:
            if type(child) is Tree:
                res = self._find_root(tag_name, child)
                if res:
                    return res

    def tree_to_text(self, item):
        """
        Проход по дереву с выводом текста
        """
        text = '' if item != self.root else self._tag_regular(self.title.data[0], self.title)
        for child in item.data:
            text += child if type(child) is str else self.tree_to_text(child)

        # result = f"\n<{item.tag} tags={item.attr}>\n{self._tag_regular(text, item)}\n</{item.tag}>\n"
        result = self._tag_regular(text, item)

        if item == self.root:
            return re.sub("\n{3,}", "\n\n", re.sub(r"\[\n+", "[\n", re.sub(r"^\n+", "", result)))

        return result

    def _fix_hyperlink(self, link):
        if link[:4] == "http":
            return link

        protocol, domain = [i for i in self.config["page"]["url"].split("/") if len(i)][:2]

        if link[:2] == "//":
            return protocol + link
        if link[:1] == "/":
            return f"{protocol}//{domain}" + link

        return ''

    def _tag_regular(self, text_par, tag):
        template = self.config["parser"]["template_default"]
        for item in self.config["parser"]["templates"]:
            if tag.tag in item["tags"]:
                template = item["template"]

        if 'attr' in tag:
            if 'href' in tag.attr:
                link = self._fix_hyperlink(tag.attr['href'])
                if text_par == "" or link == "":
                    return ""

                template = re.sub('{href}', link, template)
                text_par = re.sub("^\n+", "", re.sub(r"\n+$", "", text_par))

            if 'src' in tag.attr:
                link = self._fix_hyperlink(tag.attr['src'])
                if link == "":
                    return ""

                template = re.sub('{src}', link, template)

        if '{href}' in template or '{src}' in template:
            return ''

        template = re.sub('{text}', text_par, template)
        return template

    def _str_format(self, text):
        """
        Ограничивает максимальную длину выводимого текста
        """
        res, line = '', ''

        for word in text.split():
            if len(f'{line} {word}') > self.config["parser"]['max_length_line']:
                res = line if res == '' else f'{res}\n{line}'
                line = word
            else:
                line = word if line == '' else f'{line} {word}'

        return line if res == '' else f'{res}\n{line}'
