import requests


class Page:
    def __init__(self, url=None, config=None):
        config = config if config else {}

        if url:
            config['url'] = url

        self.text = self.request(config)

    @staticmethod
    def request(config):
        print('# Загрузка страницы... ')

        # TODO задать количество попыток
        try:
            with requests.get(**config) as r:
                if r.status_code == 200:
                    print('- успешно')
                    return r.text

                print('- неудача')
                exit(1)
        except:
            print('- неудача')
            exit(1)
