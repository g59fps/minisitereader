import os


class File:
    path = ''
    filename = ''

    def __init__(self, path=None, config=None):
        if path:
            self.filename = path
            return

        mas = self.parse_url(config['page']['url'])

        if len(mas) > 1:
            self.path = '/'.join(mas[:-1]) + '/'
            self.filename = f'{mas[-1:][0]}.{config["file"]["output_file_format"]}'
        else:
            self.path = f'{mas[0]}/'
            self.filename = f'index.{config["file"]["output_file_format"]}'

    def parse_url(self, url):
        """
        Split по косой черте, убираем протокол (http[s]) и параметры запроса (после ?)
        """
        mas = [i for i in url.split('/') if len(i)][1:]

        filename = mas[len(mas) - 1]
        filename = filename.split('?')[0]
        mas[len(mas) - 1] = "index" if filename == "" else filename

        return mas

    def write(self, text):
        os.makedirs(self.path, mode=0o777, exist_ok=True)

        with open(f'{self.path}{self.filename}', 'w', encoding='utf-8') as file:
            file.write(text)

    def read(self):
        with open(f'{self.path}{self.filename}', 'r', encoding='utf-8') as file:
            return file.read()
