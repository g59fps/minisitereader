from .file import File
from json import loads


class Config(dict):
    # Переопределение позволяет получить доступ
    # к полям конфига первого уровня через точку
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __init__(self):
        """
        Заливаем данные конфига в объект как в словарь
        """
        cnf = File(path='config.json').read()
        self.update(loads(cnf))

        super().__init__()
