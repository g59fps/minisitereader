import sys

from app_data.file import File
from app_data.page import Page
from app_data.config import Config
from app_data.parser import Parser

if __name__ == '__main__':
    conf = Config()

    if len(sys.argv) > 1:
        page = Page(url=sys.argv[1], config=conf['page'])
    else:
        page = Page(config=conf['page'])

    with open('index.html', 'w', encoding='utf-8') as file:
        file.write(page.text)

    parser = Parser(page.text, conf)

    print(f"\n# Поиск блока статьи по метке:")
    parser.find_root()
    result = parser.tree_to_text(parser.root)

    file = File(config=conf)
    file.write(str(result))
    print(f"\n# Страница записана в файл:\n./{file.path}{file.filename}")