## MiniSiteReader

перед запуском проекта необходимо установить зависимости:
```
pip install -r requirements.txt
``` 


### описание проекта

Проект представляет собой скрипт разбора заданной страницы произвольного 
сайта для сохранения только полезной информации без рекламных и прочих 
лишних составляющих.

По условиям написания проекта нельзя было использовать готовые библиотеки 
для парсинга сайтов: `lxml`, её производные и аналоги.
 
По этой причине за "базу" была использована стандартная библиотека 
`html.parser` на основе `parser.Tree` расширением методов класса:

``` 
Parser(HTMLParser):
    def handle_starttag(self, tag, attrs): pass
    def handle_endtag(self, tag): pass
    def handle_data(self, data): pass
```

Из-за возможных ошибок на сайте (не всегда закрываются 
теги и прочее) - в парсере в начале собирается дерево 
документа (упрощенный аналог DOM с сохранением 
тега, некоторых его атрибутов и внутреннего текста)


### пояснения настроек файла `config.json`

- раздел `page` переносится в параметры запроса `requests.get`
  - `url` адрес страницы по умолчанию (если не задан при запуске скрипта)
- раздел `file` только задает формат выходного файла
- раздел `parser`:
  - `ignore_line_tags` список игнорируемых тегов при построении дерева (временно отключён за ненадобностью)
  - `root` список тегов, которые могут быть на странице родительским тегом статьи
  - `cancel_tags` список игнорируемых тегов при генерации текста статьи
  - `max_length_line` максимально допустимая длина строки (для ссылок игнорируется)
  - `templates` и `template_default` шаблоны для разных тегов или по умолчанию 
    - `{text}` определяет положение основного текста внутри тега
    - `{href}`, `{src}` соответственные параметры или атрибуты тега (определены только эти поля для шаблонов)
    

### Скрипт протестирован на следующих сайтах: 

- lenta.ru: 
[1](https://lenta.ru/articles/2015/03/04/freak/)
/
[2](https://lenta.ru/articles/2019/08/16/extraordinarylanding/)

- news.mail.ru:
[1](https://news.mail.ru/society/38421767/)
/
[2](https://news.mail.ru/society/38418081/)

- news.yandex.ru: 
[1](https://news.yandex.ru/story/Google_Intel_i_Microsoft_sozdayut_novyj_konsorcium_dlya_zashhity_dannykh--2bb60bf26ddc2c02c000d62b150df9f3?lr=48&lang=ru&persistent_id=71875902&rubric=personal_feed&from=index)
/
[2](https://news.yandex.ru/story/Rossiya_vpervye_zapustila_v_kosmos_robota_Fedora--9e0cd9b95ca45c65037e0913bb4509d3?lr=48&stid=WLd1S9f6nBKZE90DbIKX&persistent_id=71860918&lang=ru&rubric=index&from=story)

- vesti.ru:
[1](https://www.vesti.ru/doc.html?id=3181216)
/
[2](https://hitech.vesti.ru/article/1225610/)
/
[3](https://hitech.vesti.ru/article/1222827/)

- gazeta.ru:
[1](https://www.gazeta.ru/tech/2019/08/21/12591931/quit_china.shtml)
/
[2](https://www.gazeta.ru/tech/2019/08/17/12582601/racism_in_google.shtml)


### Работа скрипта:

- чтение файла конфигурации: с помощью `from app_data.config import Config`
- получение страницы `from app_data.page import Page` с помощью библиотеки 
`requests` (находится в файле зависимостей проекта: `requirements.txt`)
- анализ страницы и вывод полезной информации модулем 
`from app_data.parser import Parser`
- вывод информации в файл модулем `from app_data.file import File`


### Варианты развития проекта:

 - отслеживание backgroud (background-image если быть более точным) тегов для сохранения на странице изображений
 - более тонкая настройка параметров скрипта
 - убрать обязательность учитывания максимальной длины строки для заголовков (для ссылок это правило уже выполняется, ввиду неадекватного поведения любого текстового редактора в случае с "рваными" ссылками)